package com.stibo.demo.report.service;

import com.stibo.demo.report.model.Attribute;
import com.stibo.demo.report.model.AttributeGroup;
import com.stibo.demo.report.model.AttributeLink;
import com.stibo.demo.report.model.Datastandard;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ReportService {
    private static final String INDENT = "  ";
    private static final String NEWLINE= "\r";
    private static final String NON_OPTIONAL_FIELD_MARKER = "*";
    private static final String MULTI_VALUE_FIELD_MARKER = "[]";
    private static final String OPEN_BRACES = "{";
    private static final String CLOSE_BRACES = "}";
    private static final List<String> HEADER = Arrays.asList("Category Name", "Attribute Name", "Description", "Type", "Groups");

    public Stream<Stream<String>> report(Datastandard datastandard, String categoryId) {
        // The Datastandard class has no field validation like: @NotNull for crucial fields. I'd provide such validation in the model to prevent too much 'if-ology' in this service.
        List<List<String>> result = new ArrayList<>();

        result.add(HEADER);

        datastandard.getCategories().stream().filter(cat -> cat.getId().equals(categoryId)).forEach((cat) -> {
            final String categoryName = cat.getName();

            cat.getAttributeLinks().forEach((attributeLink -> {
                // The next line could also include saving the already found attributes to a Map. On the next iteration try to find an attribute in the map first, then iterate the list. It depends on the input data character though.
                final Attribute attr = datastandard.getAttributes().stream().filter(attribute -> attribute.getId().equals(attributeLink.getId())).findFirst().get();

                final String attrName = this.generateAttributeName(attributeLink, attr);
                final String attrType = this.generateType(datastandard.getAttributes(), attr);
                final String groups = this.generateGroups(attr, datastandard.getAttributeGroups());

                result.add(Arrays.asList(categoryName, attrName, attr.getDescription(), attrType, groups));
            }));
        });

        return result.stream().map((el) -> el.stream()).collect(Collectors.toList()).stream();
    }

    private String generateAttributeName(AttributeLink attributeLink, Attribute attribute) {
        return attributeLink.getOptional() ? attribute.getName() : attribute.getName() + NON_OPTIONAL_FIELD_MARKER;
    }

    private String generateType(List<Attribute> attributes, Attribute attr) {

        final StringBuilder attributeLinkBuilder = new StringBuilder();
        attributeLinkBuilder.append(attr.getType().getId());

        this.appendNestedAttributes(attributeLinkBuilder, attr.getAttributeLinks(), attributes, new StringBuilder());

        if(attr.getType().getMultiValue()) {
            attributeLinkBuilder.append(MULTI_VALUE_FIELD_MARKER);
        }

        return attributeLinkBuilder.toString();
    }

    private void appendNestedAttributes(StringBuilder attributeLinkBuilder, List<AttributeLink> attributeLinks, List<Attribute> attributes, StringBuilder indentBuilder) {
        if (attributeLinks != null && attributeLinks.size() > 0) {
            this.increaseIndent(indentBuilder, INDENT);
            attributeLinkBuilder.append(OPEN_BRACES).append(NEWLINE).append(indentBuilder.toString());

            attributeLinks.stream().forEach((attributeLink -> {
                Attribute linkedAttribute = attributes.stream().filter(attribute -> attribute.getId().equals(attributeLink.getId())).findFirst().get();

                attributeLinkBuilder.append(attributeLink.getOptional() ? linkedAttribute.getName() : linkedAttribute.getName() + NON_OPTIONAL_FIELD_MARKER).append(": ").append(linkedAttribute.getType().getId());
                this.appendNestedAttributes(attributeLinkBuilder, linkedAttribute.getAttributeLinks(), attributes, indentBuilder);
                attributeLinkBuilder.append(NEWLINE).append(indentBuilder.toString());
            }));
            attributeLinkBuilder.delete(attributeLinkBuilder.length() - (NEWLINE.length() + indentBuilder.length()), attributeLinkBuilder.length()); // Remove the last newline

            this.decreaseIndent(indentBuilder, INDENT);
            attributeLinkBuilder.append(NEWLINE).append(indentBuilder.toString()).append(CLOSE_BRACES);
        }
    }

    private String generateGroups(Attribute attribute, List<AttributeGroup> attributeGroups) {
        final StringBuilder groupsBuilder = new StringBuilder();

        attribute.getGroupIds().stream().forEach((grId) ->
                groupsBuilder.append(attributeGroups.stream().filter(atrGroup -> atrGroup.getId().equals(grId)).findFirst().get().getName()).append(NEWLINE)
        );

        return groupsBuilder.toString();
    }

    private void increaseIndent(StringBuilder indentBuilder, String indent) {
        indentBuilder.append(indent);
    }

    private void decreaseIndent(StringBuilder indentBuilder, String indent) {
        indentBuilder.delete(indentBuilder.length() - indent.length(), indentBuilder.length());
    }
}

