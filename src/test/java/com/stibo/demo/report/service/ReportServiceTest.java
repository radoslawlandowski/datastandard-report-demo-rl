package com.stibo.demo.report.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stibo.demo.report.model.Datastandard;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class) // Should that really be a SpringRunner? Could it be a Mockito Runner considering it's a unit test thus no need to spin up entire Spring context?
@ContextConfiguration(classes = {ReportService.class, ObjectMapper.class})
public class ReportServiceTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ReportService reportService;

    private Datastandard datastandard;

    @Before
    public void before() throws IOException {
        InputStream stream = getClass().getClassLoader().getResourceAsStream("datastandard.json");
        this.datastandard = objectMapper.readValue(stream, Datastandard.class);
    }

    // For the sake of simplicity and ease of development I implemented a test case per datastandard.json example.
    // A more 'production ready' tests would be more atomic, concentrating on separate features of service rather than
    // data examples, like: "When field is not optional, a non-optional marker is added", "When field is multivalue, a multivalue marker is added" etc.
    @Test
    public void testLeafReport() {
        List<List<String>> report = reportService.report(datastandard, "leaf").map(row -> row.collect(toList())).collect(toList());

        assertThat(report.size()).isEqualTo(2);

        List<String> leafReport = report.get(1);

        assertThat(leafReport.size()).isEqualTo(5);
        assertThat(leafReport.get(0)).isEqualTo("Leaf");
        assertThat(leafReport.get(1)).isEqualTo("Composite Value");
        assertThat(leafReport.get(2)).isEqualTo("Composite Value Description");

        String type = "composite{\r"+
                "  Nested Value*: integer{\r"+
                "    My Attr*: myattr{\r"+
                "      Nested NestedAttr*: nestednestedattrtype\r"+
                "    }\r"+
                "    Nested NestedAttr*: nestednestedattrtype\r"+
                "  }\r"+
                "  My Attr*: myattr{\r"+
                "    Nested NestedAttr*: nestednestedattrtype\r"+
                "  }\r"+
                "}[]";
        assertThat(leafReport.get(3)).isEqualTo(type);
        assertThat(leafReport.get(4)).isEqualTo("All\rComplex\r");
    }

    @Test
    public void testRootReport() {
        List<List<String>> report = reportService.report(datastandard, "root").map(row -> row.collect(toList())).collect(toList());

        assertThat(report.size()).isEqualTo(2);

        List<String> leafReport = report.get(1);

        assertThat(leafReport.size()).isEqualTo(5);
        assertThat(leafReport.get(0)).isEqualTo("Root");
        assertThat(leafReport.get(1)).isEqualTo("String Value*");
        assertThat(leafReport.get(2)).isEqualTo(null);
        assertThat(leafReport.get(3)).isEqualTo("string");
        assertThat(leafReport.get(4)).isEqualTo("All\r");
    }
}
